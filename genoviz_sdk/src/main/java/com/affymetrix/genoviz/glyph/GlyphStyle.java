/**
 * Copyright (c) 1998-2005 Affymetrix, Inc.
 *
 * Licensed under the Common Public License, Version 1.0 (the "License"). A copy
 * of the license must be included with any distribution of this source code.
 * Distributions from Affymetrix, Inc., place this in the IGB_LICENSE.html file.
 *
 * The license is also available at http://www.opensource.org/licenses/cpl.php
 */
package com.affymetrix.genoviz.glyph;

import java.awt.Color;
import java.awt.Font;

/**
 * A Glyph Style is associated with each glyph, keeping its background color,
 * foreground color and Font. In combination with the {@link GlyphStyleFactory},
 * it uses the Flyweight pattern. Only one object exists for each used
 * combination of colors and fonts.
 *
 * <p>
 * There is no empty constructor because GlyphStyle is immutable. It has no set
 * accessors. The get accessors return pointers. However, the properties are
 * each immutable objects themselves. Hence, the properties of a GlyphStyle
 * cannot be changed.
 */
public class GlyphStyle {

    private final Color background_color;
    private final Color foreground_color;
    private final Font fnt;

    public GlyphStyle(Color foreground_color, Color background_color, Font fnt) {
        if (foreground_color == null || background_color == null || fnt == null) {
            throw new NullPointerException("Can't make GlyphStyle with null constructor argument.");
        }
        this.background_color = background_color;
        this.foreground_color = foreground_color;
        this.fnt = fnt;
    }

    public Color getBackgroundColor() {
        return background_color;
    }

    public Color getForegroundColor() {
        return foreground_color;
    }

    public Font getFont() {
        return fnt;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 79 * hash + (this.background_color != null ? this.background_color.hashCode() : 0);
        hash = 79 * hash + (this.foreground_color != null ? this.foreground_color.hashCode() : 0);
        hash = 79 * hash + (this.fnt != null ? this.fnt.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final GlyphStyle other = (GlyphStyle) obj;
        if (this.background_color != other.background_color && (this.background_color == null || !this.background_color.equals(other.background_color))) {
            return false;
        }
        if (this.foreground_color != other.foreground_color && (this.foreground_color == null || !this.foreground_color.equals(other.foreground_color))) {
            return false;
        }
        return this.fnt == other.fnt || (this.fnt != null && this.fnt.equals(other.fnt));
    }
}
